/*
 * dataIhm.h
 *
 *  Created on: May 31, 2021
 *      Author: Evann Clavier
 */

#ifndef DATAIHM_H_
#define DATAIHM_H_

/* Includes ------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "main.h"

/* USER CODE BEGIN Private defines */
#define NB_SONAR 4
#define MSG_CPU_SIZE	sizeof(msgCpu)	//48 bytes
#define CFG 0xB87D935										/*Main -> Sensor */
#define CLK 0xB87D9FF		/*mode : Buried Object or Shield*/										/*Main -> Sensor */
#define PSS 0xB88123B		/*sonar position*/						/*Main -> Sensor */
/* USER CODE END Private defines */

/* USER CODE BEGIN Private variables */
typedef struct {
	uint32_t date;
	uint8_t type[3];
	uint8_t num;
	union{
		uint8_t c[40];
		float f[10];
		double d[5];
	}data;
}msgCpu;

typedef struct {
	int inputID,outputID;
	int maxInput;
	msgCpu msgTab[64];


}msgBuff; //VB

/* USER CODE END Private defines */

/* USER CODE BEGIN function prototype defines */
int checkMessage();
void sendMsgToIhm(msgCpu* mToSend,uint32_t date, uint8_t t1, uint8_t t2, uint8_t t3, uint8_t num, float* data);
void rcvMsgFromCpu (msgCpu *PmsgCpu);
uint32_t getDatemsUI32(void);
void rcvMsgFromIhm(msgCpu *PmsgIhm);
void toTransmit(msgCpu * msgPtr);
void Transmit(msgCpu * msgPtr);
void tryTransmit(void);
void trySendPos(int intervalSendPos);
void sendPos();
void copyMsg(msgCpu * from, msgCpu * to);

/* USER CODE END function prototype defines */

#endif /* DTAIHM_H_ */
