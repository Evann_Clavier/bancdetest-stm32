/*
 * stepper.h
 *
 *  Created on: 26 Apr 2021
 *      Author: Samira
 */
/* USER CODE BEGIN 0 */

#include "main.h"
#include "gpio.h"
#include "dataIhm.h"

#ifndef INC_STEPPER_H_
#define INC_STEPPER_H_
#define BACKWARD 0	//VB
#define FORWARD 1	//VB
#define LEFTY 0	//VB
#define RIGHTY 1	//VB
#define UPZ 0	//VB
#define DOWNZ 1	//VB
#define CLOCKWISER 0	//VB
#define ANTICLOCKWISER 1	//VB


extern unsigned int countX, countY,countZ, countR;
extern motorState X_state, Y_state, Z_state, R_state;

enum end_type_enum {DISTANCE,SWITCH,FORCED};
enum motor_state {None,Start,Run, End, Stop, BackStep1,BackStep2,BackStep3,AntiShift1,AntiShift2,AntiShift3,AntiShift4,AntiShift5};

void startAll_PWM(void);
void stopAll_PWM(void);

void startX_PWM(void);
void startY_PWM(void);
void startZ_PWM(void);
void startR_PWM(void);

void startX(void);
void startY(void);
void startZ(void);
void startR(void);

void stopX_PWM(void);
void ForceStopX_PWM(void);
void stopY_PWM(void);
void ForceStopY_PWM(void);
void stopZ_PWM(void);
void ForceStopZ_PWM(void);
void stopR_PWM(void);
void ForceStopR_PWM(void);

int X_step_to_10Um(int mm);
int X_mm_to_step(int mm);
int Y_step_to_10Um(int mm);
int Y_mm_to_step(int mm);
int Z_step_to_10Um(int mm);
int Z_mm_to_step(int mm);
int R_step_to_10Um(int mm);
int R_mm_to_step(int mm);

void counter_reset_all(void);

void forwardX (void);
void backwardX (void);
void reverseX(void);
void reverseX1(void);
void reverseX2(void);

void leftY(void);
void rightY(void);
void reverseY(void);

void upZ(void);
void downZ(void);
void reverseZ(void);
void reverseZ1(void);
void reverseZ2(void);

void clockwiseR(void);
void anticlockwiseR(void);
void reverseR(void);

void accelerateX(void);
void decelerateX(void);

void accelerateY(void);
void decelerateY(void);

void accelerateZ(void);
void decelerateZ(void);

void accelerateR(void);
void decelerateR(void);

void memorisePosition(void);

void set_speed_all(float Vx, float Vy, float Vz, float Vr);
void set_speed_x(float Vx);
void set_speed_y(float Vy);
void set_speed_z(float Vz);
void set_speed_r(float Vr);
void reset_distances(void);

void X_run(_Bool dir, int end_type ,int dist, int vitesse );
void Y_run(_Bool dir, int end_type ,int dist, int vitesse );
void Z_run(_Bool dir, int end_type ,int dist, int vitesse );

void X_sate_machine(void);
void Y_sate_machine(void);
void Z_sate_machine(void);
void manualState(msgCpu* PmsgIhm);
_Bool isInSecureMode(void);

void goToSinglePosition(void);
void goTo_Position(void);
void All_runPosition(void);
void goTo_SinglePosition_old(msgCpu* PmsgIhm);
void goToNextPosition(msgCpu* PmsgIhm);
_Bool checkPosition(void);
void manualControl(msgCpu* PmsgIhm);
void goTo_Zero(void);

#endif /* INC_STEPPER_H_ */

/* USER CODE END 0 */
