/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stm32f4xx.h"
//#include "dataIhm.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
extern UART_HandleTypeDef huart2;
enum bdtFlags {
	stopped,moving,message,manual,switchActive
};
enum message_type {
	URG,CLK,PTS,OFS,BUS,DIR,GOTOZERO,GOTOSINGLEPOSITION,OFFSET1,OFFSET2
};
enum mainStateEnum {Waiting, Mouving, Mouved , Manual};

typedef struct {
	int state;
	_Bool dir;
	int end_type;
	int dist;
	int vitesseTarget;
	int posInStep;
	int vitesseCurrent;
	int vitesseCanStart;
	int vitesseMax;
	int vitesseIncrement;
	_Bool vitesseToChangeFlag;
}motorState; //VB



/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
#define IHM_UART	huart2
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void delay_us (uint16_t us, TIM_HandleTypeDef timer);
int _write(int file, char *ptr, int len);
void checkSwitches(void);

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
/* USER CODE BEGIN Private defines */
#define X_STEPS_PER_MM 31.48 //15.45
#define Y_STEPS_PER_MM 13.33
#define Z_STEPS_PER_MM 213.5
#define R_STEPS_PER_REV 94.11 //222.22
#define MAX_STEPS
#define MIN_STEPS

enum SW_FLAG_ENUM {
	SW_X1SEC=1,SW_X2SEC,SW_X1MAX,SW_X1MIN,SW_X2MAX,SW_X2MIN,SW_YMAX,SW_YMIN,SW_Z1MAX,SW_Z1MIN,SW_Z2MAX,SW_Z2MIN
}; //VB
#define SW_FLAG_LEN SW_Z2MIN+1

//#define SW_X1SEC 1
//#define SW_X2SEC 2
//#define SW_X1MAX 3
//#define SW_X1MIN 4
//#define SW_X2MAX 5
//#define SW_X2MIN 6
//#define SW_YMAX 7
//#define SW_YMIN 8
//#define SW_Z1MAX 9
//#define SW_Z1MIN 10
//#define SW_Z2MAX 11
//#define SW_Z2MIN 12
//VB

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
