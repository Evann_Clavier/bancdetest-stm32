/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file    stm32f4xx_it.c
 * @brief   Interrupt Service Routines.
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "gpio.h"
#include "stepper.h"
#include "dataIhm.h"
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */
HAL_StatusTypeDef status;
/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
extern msgCpu msg;
uint32_t tick = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim5;
extern UART_HandleTypeDef huart2;
/* USER CODE BEGIN EV */
extern unsigned int countX, countY, countZ, countR;
extern uint32_t stepsX, stepsY, stepsZ, stepsR;
extern msgCpu messageIhm, msgPos;
extern uint8_t * BufIt2DataPU8;
extern uint32_t ihmMsgLenU32, startTime;
extern float x_dist, y_dist, z_dist, r_dist;
extern unsigned int distanceX, distanceY, distanceZ;
extern uint8_t up, down, forward, back, left, right, clockwise, anticlockwise;
extern enum bdtFlags flag;
extern uint8_t sw;
extern _Bool X_mouving, Y_mouving, Z_mouving, R_mouving;
extern volatile _Bool X_limited, Y_limited, Z_limited, R_limited;
extern volatile _Bool _swFlagTab[12];
uint32_t tmpBugmot;
int tmpCpt,tmpCptZmin1,tmpCptZmin2;

//extern _Bool sw_X1sec,sw_X2sec,sw_X1min,sw_X1max,sw_X2min,
//		sw_X2max,sw_Ymin,sw_Ymax,sw_Z1min,sw_Z2min,sw_Z1max,sw_Z2max;
/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M4 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
 * @brief This function handles Non maskable interrupt.
 */
void NMI_Handler(void)
{
	/* USER CODE BEGIN NonMaskableInt_IRQn 0 */

	/* USER CODE END NonMaskableInt_IRQn 0 */
	/* USER CODE BEGIN NonMaskableInt_IRQn 1 */
	while (1)
	{
	}
	/* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
 * @brief This function handles Hard fault interrupt.
 */
void HardFault_Handler(void)
{
	/* USER CODE BEGIN HardFault_IRQn 0 */

	/* USER CODE END HardFault_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_HardFault_IRQn 0 */
		/* USER CODE END W1_HardFault_IRQn 0 */
	}
}

/**
 * @brief This function handles Memory management fault.
 */
void MemManage_Handler(void)
{
	/* USER CODE BEGIN MemoryManagement_IRQn 0 */

	/* USER CODE END MemoryManagement_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
		/* USER CODE END W1_MemoryManagement_IRQn 0 */
	}
}

/**
 * @brief This function handles Pre-fetch fault, memory access fault.
 */
void BusFault_Handler(void)
{
	/* USER CODE BEGIN BusFault_IRQn 0 */

	/* USER CODE END BusFault_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_BusFault_IRQn 0 */
		/* USER CODE END W1_BusFault_IRQn 0 */
	}
}

/**
 * @brief This function handles Undefined instruction or illegal state.
 */
void UsageFault_Handler(void)
{
	/* USER CODE BEGIN UsageFault_IRQn 0 */

	/* USER CODE END UsageFault_IRQn 0 */
	while (1)
	{
		/* USER CODE BEGIN W1_UsageFault_IRQn 0 */
		/* USER CODE END W1_UsageFault_IRQn 0 */
	}
}

/**
 * @brief This function handles System service call via SWI instruction.
 */
void SVC_Handler(void)
{
	/* USER CODE BEGIN SVCall_IRQn 0 */

	/* USER CODE END SVCall_IRQn 0 */
	/* USER CODE BEGIN SVCall_IRQn 1 */

	/* USER CODE END SVCall_IRQn 1 */
}

/**
 * @brief This function handles Debug monitor.
 */
void DebugMon_Handler(void)
{
	/* USER CODE BEGIN DebugMonitor_IRQn 0 */

	/* USER CODE END DebugMonitor_IRQn 0 */
	/* USER CODE BEGIN DebugMonitor_IRQn 1 */

	/* USER CODE END DebugMonitor_IRQn 1 */
}

/**
 * @brief This function handles Pendable request for system service.
 */
void PendSV_Handler(void)
{
	/* USER CODE BEGIN PendSV_IRQn 0 */

	/* USER CODE END PendSV_IRQn 0 */
	/* USER CODE BEGIN PendSV_IRQn 1 */

	/* USER CODE END PendSV_IRQn 1 */
}

/**
 * @brief This function handles System tick timer.
 */
void SysTick_Handler(void)
{
	/* USER CODE BEGIN SysTick_IRQn 0 */

	/* USER CODE END SysTick_IRQn 0 */
	HAL_IncTick();
	/* USER CODE BEGIN SysTick_IRQn 1 */
//		int delay = HAL_GetTick() - tick;
//		if(delay >= 2000) {
//			// Send position to Operator
//			msgPos.date = getDatemsUI32();
//			msgPos.data.f[0] = x_dist;
//			msgPos.data.f[1] = y_dist;
//			msgPos.data.f[2] = z_dist;
//			msgPos.data.f[3] = r_dist;
//			HAL_UART_Transmit(&huart2, (uint8_t *)&msgPos, MSG_CPU_SIZE,50);
//			tick = HAL_GetTick();
//		}
	/* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
 * @brief This function handles EXTI line0 interrupt.
 */
void EXTI0_IRQHandler(void)
{
	/* USER CODE BEGIN EXTI0_IRQn 0 */

	/* USER CODE END EXTI0_IRQn 0 */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);
	/* USER CODE BEGIN EXTI0_IRQn 1 */
	//	}
	/* USER CODE END EXTI0_IRQn 1 */
}

/**
 * @brief This function handles EXTI line1 interrupt.
 */
void EXTI1_IRQHandler(void)
{
	/* USER CODE BEGIN EXTI1_IRQn 0 */

	/* USER CODE END EXTI1_IRQn 0 */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_1);
	/* USER CODE BEGIN EXTI1_IRQn 1 */

	/* USER CODE END EXTI1_IRQn 1 */
}

/**
 * @brief This function handles EXTI line2 interrupt.
 */
void EXTI2_IRQHandler(void)
{
	/* USER CODE BEGIN EXTI2_IRQn 0 */

	/* USER CODE END EXTI2_IRQn 0 */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_2);
	/* USER CODE BEGIN EXTI2_IRQn 1 */

	/* USER CODE END EXTI2_IRQn 1 */
}

/**
 * @brief This function handles EXTI line[9:5] interrupts.
 */
void EXTI9_5_IRQHandler(void)
{
	/* USER CODE BEGIN EXTI9_5_IRQn 0 */

	/* USER CODE END EXTI9_5_IRQn 0 */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_7);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_8);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_9);
	/* USER CODE BEGIN EXTI9_5_IRQn 1 */

	/* USER CODE END EXTI9_5_IRQn 1 */
}

/**
 * @brief This function handles TIM2 global interrupt.
 */
void TIM2_IRQHandler(void)
{
	/* USER CODE BEGIN TIM2_IRQn 0 */

	/* USER CODE END TIM2_IRQn 0 */
	HAL_TIM_IRQHandler(&htim2);
	/* USER CODE BEGIN TIM2_IRQn 1 */
	countX++;
	tmpCpt++;
//	if(forward) {
//		distanceX ++;
//	} else distanceX --;
//	x_dist = (distanceX*3400)/108;
	if (forward){ X_state.posInStep++;}
	else { X_state.posInStep--;}
	if(countX >= stepsX && X_limited) {
		ForceStopX_PWM();

	}
	/* USER CODE END TIM2_IRQn 1 */
}

/**
 * @brief This function handles TIM3 global interrupt.
 */
void TIM3_IRQHandler(void)
{
	/* USER CODE BEGIN TIM3_IRQn 0 */

	/* USER CODE END TIM3_IRQn 0 */
	HAL_TIM_IRQHandler(&htim3);
	/* USER CODE BEGIN TIM3_IRQn 1 */
	countY++;
//	if(right) {
//		distanceY ++;
//	} else distanceY --;
//		y_dist = distanceY/Y_STEPS_PER_MM;
	if (right){ Y_state.posInStep++;}
		else { Y_state.posInStep--;}
	if(countY >= stepsY && Y_limited) {
		ForceStopY_PWM();
		//Y_mouving=0;
	}
	/* USER CODE END TIM3_IRQn 1 */
}

/**
 * @brief This function handles TIM4 global interrupt.
 */
void TIM4_IRQHandler(void)
{
	/* USER CODE BEGIN TIM4_IRQn 0 */

	/* USER CODE END TIM4_IRQn 0 */
	HAL_TIM_IRQHandler(&htim4);
	/* USER CODE BEGIN TIM4_IRQn 1 */
	countZ++;
//	if(down) {
//		distanceZ ++;
//	} else distanceZ --;
//		z_dist = distanceZ/Z_STEPS_PER_MM;
	if (down){ Z_state.posInStep++;}
			else { Z_state.posInStep--;}
	if(countZ >= stepsZ && Z_limited) {
		ForceStopZ_PWM();}
	/* USER CODE END TIM4_IRQn 1 */
}

/**
 * @brief This function handles USART2 global interrupt.
 */
void USART2_IRQHandler(void)
{
	/* USER CODE BEGIN USART2_IRQn 0 */
	//	uartReady = 1;
	/* USER CODE END USART2_IRQn 0 */
	HAL_UART_IRQHandler(&huart2);
	/* USER CODE BEGIN USART2_IRQn 1 */

	/* USER CODE END USART2_IRQn 1 */
}

/**
 * @brief This function handles EXTI line[15:10] interrupts.
 */
void EXTI15_10_IRQHandler(void)
{
	/* USER CODE BEGIN EXTI15_10_IRQn 0 */

	/* USER CODE END EXTI15_10_IRQn 0 */
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_10);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_11);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_12);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_13);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_14);
	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_15);
	/* USER CODE BEGIN EXTI15_10_IRQn 1 */

	/* USER CODE END EXTI15_10_IRQn 1 */
}

/**
 * @brief This function handles TIM5 global interrupt.
 */
void TIM5_IRQHandler(void)
{
	/* USER CODE BEGIN TIM5_IRQn 0 */

	/* USER CODE END TIM5_IRQn 0 */
	HAL_TIM_IRQHandler(&htim5);
	/* USER CODE BEGIN TIM5_IRQn 1 */
	countR++;
	if (R_state.dir){ R_state.posInStep++;}
	else { R_state.posInStep--;}
	if(countR >= stepsR && R_limited) { stopR_PWM();}
	/* USER CODE END TIM5_IRQn 1 */
}

/* USER CODE BEGIN 1 */
/*	End of travel switch interrupts - switches connected as NC */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{	//tmpBugmot=HAL_GetTick();  // only for debug

	//flag |= switchActive;
	switch(GPIO_Pin) {
	case GPIO_PIN_0:		// Security Stop X1 (Tout au bout)
		_swFlagTab[SW_X1SEC]=1;
		stopX_PWM();
		break;
	case GPIO_PIN_1:		// Security Stop X2 (Tout au bout)
		_swFlagTab[SW_X2SEC]=1;
		stopX_PWM();
		break;
	case GPIO_PIN_7:		// X1 Max Limit

		_swFlagTab[SW_X1MAX]=1;
		stopX_PWM();
		break;
	case GPIO_PIN_8:		// X1 Min Limit

		_swFlagTab[SW_X1MIN]=1;
		stopX_PWM();
		break;
	case GPIO_PIN_9:		// X2 Max Limit

		_swFlagTab[SW_X2MAX]=1;
		stopX_PWM();
		break;
	case GPIO_PIN_10:		// X2 Min Limit

		_swFlagTab[SW_X2MIN]=1;
		stopX_PWM();
		break;
	case GPIO_PIN_11:		// Y Max Limit
		stopY_PWM();
		_swFlagTab[SW_YMAX]=1;
		break;
	case GPIO_PIN_12:		// Y Min Limit
		stopY_PWM();
		_swFlagTab[SW_YMIN]=1;
		break;
	case GPIO_PIN_13:		// Z1 Max Limit
		stopZ_PWM();
		_swFlagTab[SW_Z1MAX]=1;
		break;
	case GPIO_PIN_14:		// Z1 Min Limit
		stopZ_PWM();
		_swFlagTab[SW_Z1MIN]=1;
		tmpCptZmin1++;
		break;
	case GPIO_PIN_15:		// Z2 Max Limit
		stopZ_PWM();
		_swFlagTab[SW_Z2MAX]=1;
		break;
	case GPIO_PIN_2:		// Z2 Min Limit
		stopZ_PWM();
		_swFlagTab[SW_Z2MIN]=1;
		tmpCptZmin2++;
		break;
	}
}

/* USER CODE END 1 */
