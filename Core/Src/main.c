/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
 * All rights reserved.</center></h2>
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *                        opensource.org/licenses/BSD-3-Clause
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stepper.h"
#include <stdlib.h>
#include <stdio.h>
#include <usart.h>
#include <string.h>
#include "math.h"
#include "dataIhm.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
enum bdtFlags flag;



//Motors
unsigned int countX=0, countY=0, countZ=0, countR=0;
uint32_t stepsX = 0, stepsY = 0, stepsZ = 0, stepsR = 0;
float id = 0, X = 0, Y = 0, Z = 0, psi = 0, Vx = 0, Vy = 0, Vz = 0, Vr = 0, x_dist = 0, y_dist = 0, z_dist = 0, r_dist = 0;
int x_speed = 0, y_speed = 0, z_speed = 0, r_speed = 0, msgCode = 0;
uint8_t up, down, forward, back, left, right, clockwise, anticlockwise;
unsigned int distanceX = 0, distanceY = 0, distanceZ = 0;
_Bool X_mouving=0, Y_mouving=0, Z_mouving=0, R_mouving=0,
		X_limited=1, Y_limited=1, Z_limited=1, R_limited=1,
		flagRxFromIhm=0;


motorState X_state={0,0,0,0,0,0,0,30,100,10,0}, Y_state={0,0,0,0,0,0,0,50,150,10,0}, Z_state={0,0,0,0,0,0,0,10,20,4,0}, R_state={0,0,0,0,0,0,0,20,30,10,0};



uint8_t sw = 0;




//Communication
msgCpu messageFromIhm,messageActiveCmd, msgPos, msg;
msgBuff msgBuffer={0,0,64};
uint8_t * BufIt2DataPU8;
uint8_t uart1RcvMessSizeU8 = 0;
uint32_t ihmMsgLenU32 = 48;
uint32_t tickStartU32, startTime;
uint32_t setDateValuemsU32;
_Bool uartReady = 0, swFlagTab[SW_FLAG_LEN]={0};

// Machine state


int mainState=Waiting;



volatile
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 * @retval int
 */
int main(void)
{
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration--------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */


	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */

SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	MX_TIM4_Init();
	MX_TIM5_Init();
	MX_USART2_UART_Init();
	/* USER CODE BEGIN 2 */
//	BufIt2DataPU8=rxBuff; //VB initialisation du buffer

	BufIt2DataPU8 = (uint8_t *)&messageFromIhm; // poitage sur la meme adresse
	HAL_Delay(100);
	tickStartU32 = HAL_GetTick();	// init tickStartU32 value
	HAL_UART_Receive_IT(&huart2, (uint8_t *)BufIt2DataPU8, ihmMsgLenU32);
	/* USER CODE END 2 */

	/* Infinite loop */

	/* USER CODE BEGIN WHILE */
	while (1)
	{
		/*
		// Gestion messages UART
		if(flag & message)
		{
			flag &= ~message;
			//action en fonction du message
			rcvMsgFromIhm(&messageFromIhm);
		}
		if(flag & moving) {
			flag &= ~moving;
			goTo_NextPosition(&messageFromIhm);
			// send back received message as an acknowledge
			HAL_UART_Transmit(&IHM_UART, (uint8_t *)BufIt2DataPU8, MSG_CPU_SIZE, 50);
			HAL_UART_Receive_IT(&huart2, (uint8_t *)BufIt2DataPU8, ihmMsgLenU32);
		}
		// Gestion switches
		if(flag & switchActive){
			checkSwitches();
		}*/
		//checkSwitches();
		X_sate_machine();
		Y_sate_machine();
		Z_sate_machine();
		checkSwitches();
		trySendPos((mainState==Waiting)?10000:2000);
		tryTransmit();
		switch(mainState){
		case Waiting:
			if(flagRxFromIhm)
			{
				switch (checkMessage()){
				case URG:
					stopAll_PWM();
					flagRxFromIhm=0;
				case DIR:
					mainState=Manual;
					break;
				case GOTOZERO:
					X_run(BACKWARD, SWITCH ,0, 30);
					Y_run(LEFTY, SWITCH ,0, 50)	;
					Z_run(UPZ, SWITCH ,0, 10);
					flagRxFromIhm=0;
					mainState=Manual;
					break;
				case OFS:
					X_run(BACKWARD, SWITCH ,0, 30);
					Z_run(UPZ, SWITCH ,0, 10);
					flagRxFromIhm=0;
					mainState=OFFSET1;
					break;
				case PTS:
					goTo_Position();
					flagRxFromIhm=0;
					mainState=Mouving;
					break;
				case GOTOSINGLEPOSITION:
					goToSinglePosition();
					flagRxFromIhm=0;
					mainState=Mouving;
					break;
				default:
					rcvMsgFromIhm(&messageFromIhm);
					flagRxFromIhm=0; //VB
					break;
				}
			}
//			if(flag & moving) {
//				flag &= ~moving;
//				goTo_NextPosition(&messageFromIhm);
//				// send back received message as an acknowledge
//
//				mainState=Mouving;
//			}
			break;
		case Mouving:
			if (	(X_state.state==End || X_state.state==Stop) &&
					(Y_state.state==End || Y_state.state==Stop) &&
					(Z_state.state==End || Z_state.state==Stop) ){
				mainState=Mouved;
			}
			if(flagRxFromIhm){
				switch (checkMessage()){
				default:
					stopAll_PWM();
					mainState=Waiting;
				}
			}


			//checkSwitches();


			break;
		case Mouved:
			//			// Send acknowledge

			messageActiveCmd.date= getDatemsUI32();
			toTransmit(&messageActiveCmd);

//			HAL_UART_Transmit(&IHM_UART, (uint8_t *)&messageActiveCmd, MSG_CPU_SIZE, 50);
//			HAL_UART_Receive_IT(&huart2, (uint8_t *)BufIt2DataPU8, ihmMsgLenU32);
			mainState=Waiting;
			break;
		case Manual:
			if(flagRxFromIhm){
				switch (checkMessage()){
				case DIR:
					mainState=Manual;
					manualState(&messageFromIhm);
					flagRxFromIhm=0;
					break;
				default:
					stopAll_PWM();
					mainState=Waiting;
				}
			}
			else {
				if (X_state.state==End && Y_state.state==End && Z_state.state==End) {
					mainState=Waiting;
					X_state.posInStep=0;
					Y_state.posInStep=0;
					Z_state.posInStep=0;
					R_state.posInStep=0;
					sendPos();

				}
			}
			break;
		case OFFSET1:
			if(flagRxFromIhm){
				if (checkMessage() == DIR && messageFromIhm.num == 1){

					mainState=Manual;
					manualState(&messageFromIhm);
					flagRxFromIhm=0;
				} else {
					stopAll_PWM();
					mainState=Waiting;
				}
			}
			else {
				if (X_state.state==End  && Z_state.state==End) mainState=OFFSET2;
			}
			break;
		case OFFSET2:
			X_run(BACKWARD, FORCED ,50, 30);
			mainState=Manual;
			break;
		default:
			break;
		}

	}

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

	/* USER CODE END 3 */
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct = {0};
	RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

	/** Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/** Initializes the RCC Oscillators according to the specified parameters
	 * in the RCC_OscInitTypeDef structure.
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 4;
	RCC_OscInitStruct.PLL.PLLN = 192;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV6;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		Error_Handler();
	}

	/** Initializes the CPU, AHB and APB buses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
			|RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
	{
		Error_Handler();
	}
}

/* USER CODE BEGIN 4 */
void delay_us (uint16_t us, TIM_HandleTypeDef timer)
{
	__HAL_TIM_SET_COUNTER(&timer,0);  // set the counter value to 0
	while (__HAL_TIM_GET_COUNTER(&timer) < us);  // wait for the counter to reach the us input in the parameter
}

//Function to print using ITM - printf won't work without this
int _write(int file, char *ptr, int len)
{
	int i=0;
	for(i=0;i<len;i++)
	{ITM_SendChar((*ptr++));}
	return len;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	//flag |= message;
	//	//action en fonction du message
	//	rcvMsgFromIhm(&messageFromIhm);
	// reactivation de IT
	HAL_UART_Receive_IT(&huart2, (uint8_t *)BufIt2DataPU8, ihmMsgLenU32);
	flagRxFromIhm=1;
}

void checkSwitches(void)
{	int sw_i=0,found=0;

	//int x_back_dist=10;

	for (sw_i=0;sw_i<SW_FLAG_LEN;sw_i++){
		if(swFlagTab[sw_i]==1) {
			found=sw_i;
		}
	}


			switch(found) {
			case SW_X1SEC:

				//forwardX();
				//X_run(FORWARD, FORCED ,x_back_dist, 30);
				msg.data.c[0] = 'X';
				msg.data.c[1] = '1';
				msg.data.c[2] = 'S';
				msg.data.c[3] = 'E';
				msg.data.c[4] = 'C';
				break;
			case SW_X2SEC:

				//forwardX();
				//X_run(FORWARD, FORCED ,x_back_dist, 30);
				msg.data.c[0] = 'X';
				msg.data.c[1] = '2';
				msg.data.c[2] = 'S';
				msg.data.c[3] = 'E';
				msg.data.c[4] = 'C';
				break;
			case SW_X1MAX:

				//backwardX();
				//X_run(BACKWARD, FORCED ,x_back_dist, 30);
				msg.data.c[0] = 'X';
				msg.data.c[1] = '1';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'A';
				msg.data.c[4] = 'X';
				break;
			case SW_X1MIN:

				//forwardX();
				//X_run(FORWARD, FORCED ,x_back_dist, 30);
				msg.data.c[0] = 'X';
				msg.data.c[1] = '1';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'I';
				msg.data.c[4] = 'N';
				break;
			case SW_X2MAX:

				//backwardX();
				//X_run(BACKWARD, FORCED ,x_back_dist, 30);
				msg.data.c[0] = 'X';
				msg.data.c[1] = '2';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'A';
				msg.data.c[4] = 'X';
				break;
			case SW_X2MIN:

				//forwardX();
				//X_run(FORWARD, FORCED ,x_back_dist, 30);
				msg.data.c[0] = 'X';
				msg.data.c[1] = '2';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'I';
				msg.data.c[4] = 'N';
				break;
			case SW_YMAX:

		//		leftY();
		//		startY_PWM();
		//		HAL_Delay(300);
		//		stopY_PWM();
				msg.data.c[0] = 'Y';
				msg.data.c[1] = ' ';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'A';
				msg.data.c[4] = 'X';
				break;
			case SW_YMIN:

		//		rightY();
		//		startY_PWM();
		//		HAL_Delay(300);
		//		stopY_PWM();
				msg.data.c[0] = 'Y';
				msg.data.c[1] = ' ';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'I';
				msg.data.c[4] = 'N';
				break;
			case SW_Z1MAX:

		//		downZ();
		//		startZ_PWM();
		//		HAL_Delay(200);
		//		stopZ_PWM();
				msg.data.c[0] = 'Z';
				msg.data.c[1] = '1';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'A';
				msg.data.c[4] = 'X';
				break;
			case SW_Z1MIN:

		//		upZ();
		//		startZ_PWM();
		//		HAL_Delay(200);
		//		stopZ_PWM();
				msg.data.c[0] = 'Z';
				msg.data.c[1] = '1';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'I';
				msg.data.c[4] = 'N';
				break;
			case SW_Z2MAX:

		//		downZ();
		//		startZ_PWM();
		//		HAL_Delay(200);
		//		stopZ_PWM();
				msg.data.c[0] = 'Z';
				msg.data.c[1] = '2';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'A';
				msg.data.c[4] = 'X';
				break;
			case SW_Z2MIN:

		//		upZ();
		//		startZ_PWM();
		//		HAL_Delay(200);
		//		stopZ_PWM();
				msg.data.c[0] = 'Z';
				msg.data.c[1] = '2';
				msg.data.c[2] = 'M';
				msg.data.c[3] = 'I';
				msg.data.c[4] = 'N';
				break;
			}
			if (found!=0){
				msg.date = getDatemsUI32();
				msg.type[0] = 'F';
				msg.type[1] = 'D';
				msg.type[2] = 'C';
				msg.num = 0;

				toTransmit(&msg);
				//HAL_UART_Transmit(&huart2, (uint8_t *)&msg, MSG_CPU_SIZE, 100);
				// wait buffer is empty or time out
//				tickStartCom=HAL_GetTick();
//				while (huart2.gState !=HAL_UART_STATE_READY && tickStartCom+100> HAL_GetTick());
//				// send msg by IRQ
//				HAL_UART_Transmit_IT(&huart2, (uint8_t *)&msg, MSG_CPU_SIZE);
//
//				HAL_UART_Receive_IT(&huart2, (uint8_t *)BufIt2DataPU8, ihmMsgLenU32);
				//flag &= ~switchActive;
				swFlagTab[found]=0;
			}

}



/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void)
{
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1)
	{
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t *file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
