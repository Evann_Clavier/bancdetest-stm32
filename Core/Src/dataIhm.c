/*
 *
 *
 *  Created on: May, 2021
 *      Author: Evann Clavier
 */

#include "dataIhm.h"
#include "stepper.h"

extern uint32_t setDateValuemsU32;
extern uint32_t tickStartU32;
extern msgCpu messageFromIhm,messageActiveCmd,msgPos;
extern msgBuff msgBuffer;
volatile int offset[] = {0,0,0,0};
extern uint8_t * BufIt2DataPU8;
extern uint32_t ihmMsgLenU32;
extern int msgCode;
extern enum bdtFlags flag;
int cptSendPos=0;
uint32_t tmplastSendTick,tmpDebut,tmpFin;
int duree;
msgCpu * msgPtr;

void trySendPos(int intervalSendPos){
	static uint32_t lastSendTick;
	//int intervalSendPos=3000;
	//tmplastSendTick=HAL_GetTick();


	if (lastSendTick+intervalSendPos<HAL_GetTick()){
		sendPos();

		lastSendTick=HAL_GetTick();
	}
}

void sendPos()	{
	msgCpu pos;
	pos.type[0] = 'P';
	pos.type[1] 	= 'O';
	pos.type[2] 	= 'S';
	pos.num 		= 5;
	pos.date = getDatemsUI32();
	pos.data.f[0] = X_step_to_10Um(X_state.posInStep)/100.;
	pos.data.f[1] = Y_step_to_10Um(Y_state.posInStep)/100.;
	pos.data.f[2] = Z_step_to_10Um(Z_state.posInStep)/100.;
	pos.data.f[3] = R_step_to_10Um(R_state.posInStep)/100.;
	toTransmit(&pos);
}

void sendPosIRQ_old(msgCpu * msgPtr)	{

	msgPtr->type[0] = 'P';
	msgPtr->type[1] 	= 'O';
	msgPtr->type[2] 	= 'S';
	msgPtr->num 		= 5;

	toTransmit(msgPtr);
}

void toTransmit(msgCpu * msgPtr) {


	copyMsg(msgPtr,&(msgBuffer.msgTab[msgBuffer.inputID]));

	msgBuffer.inputID=(msgBuffer.inputID+1)%msgBuffer.maxInput;

}

void copyMsg(msgCpu * from, msgCpu * to){
	int i=0;
	for (i=0;i<3;i++){
		to->type[i] = from->type[i];
	}
	for (i=0;i<10;i++){
			to->data.f[i] = from->data.f[i];
	}
	to->num = from->num;
	to->date = from->date;

}

void tryTransmit(void){
	static uint32_t lastSendTick;
	int intervalTx=100,input=0;
	tmplastSendTick=HAL_GetTick();
	if (lastSendTick+intervalTx<HAL_GetTick())  {
		if (1){//(!isInSecureMode()) {
			input=msgBuffer.inputID;
			if ((msgBuffer.outputID != input) && (huart2.TxXferCount ==0)){
				msgPtr=&(msgBuffer.msgTab[msgBuffer.outputID]);  // only for debug live

				Transmit(&(msgBuffer.msgTab[msgBuffer.outputID]));

				msgBuffer.outputID=(msgBuffer.outputID+1)%msgBuffer.maxInput;
				lastSendTick=HAL_GetTick();
			}
		}
	}
}


void Transmit(msgCpu * msgPtr) {
	//uint32_t tickStartCom;

	// wait buffer is empty or time out
	//tickStartCom=HAL_GetTick();
	//tmpDebut=HAL_GetTick();
	while (huart2.TxXferCount !=0) ; // && tickStartCom+100> HAL_GetTick());
	// send msg by IRQ
	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msgPtr, MSG_CPU_SIZE);

	HAL_UART_Receive_IT(&huart2, (uint8_t *)BufIt2DataPU8, ihmMsgLenU32);
	//duree=HAL_GetTick()-tmpDebut;
	cptSendPos++; // only for debug live
}


int checkMessage(){
	int i;
	int find=-1;
	msgCpu *PmsgIhm=&messageFromIhm;

	// recopie du dernier message
	messageActiveCmd.date = PmsgIhm->date;
	for (i=0;i<3;i++) {
		messageActiveCmd.type[i] = PmsgIhm->type[i];
	}
	messageActiveCmd.num 	= PmsgIhm->num;
	for (i=0;i<10;i++) {
		messageActiveCmd.data.f[i] = PmsgIhm->data.f[i];
	}


	if(!strncmp((char *)PmsgIhm->type , "URG", 3)){
		find=URG;
	}

	if(!strncmp((char *)PmsgIhm->type , "CLK", 3)){
			find=CLK;
		}
	if(!strncmp((char *)PmsgIhm->type , "PTS", 3)){
		switch(PmsgIhm->num){
		case 0:
			find=GOTOSINGLEPOSITION;
			break;
		case  2:			// message from button Go to Zero
			find=GOTOZERO;
			break;
		default:
			find=PTS;
		}

		}
	if(!strncmp((char *)PmsgIhm->type , "BUS", 3)){
			find=BUS;
		}
	if(!strncmp((char *)PmsgIhm->type , "OFS", 3)){
				find=OFS;
			}
	if(!strncmp((char *)PmsgIhm->type , "DIR", 3)){
				find=DIR;
			}
	return find;


}

void rcvMsgFromIhm(msgCpu *PmsgIhm)
{
	if(!strncmp((char *)PmsgIhm->type , "URG", 3)){ //message d'urgence
		stopAll_PWM();
	}	else if(!strncmp((const char*)PmsgIhm->type , "CLK", 3)){ //message de configuration de l'horloge
		setDateValuemsU32 = PmsgIhm->date;
		tickStartU32=HAL_GetTick();
	}	else if(!strncmp((char *)PmsgIhm->type , "PTS", 3)){ //message de points futur ou nouveau
		switch(PmsgIhm->num){
		case 0:			// message from buttons Go to Start or Go to End
			messageFromIhm.date 		= PmsgIhm->date;
			messageFromIhm.type[0] 	= PmsgIhm->type[0];
			messageFromIhm.type[1] 	= PmsgIhm->type[1];
			messageFromIhm.type[2] 	= PmsgIhm->type[2];
			messageFromIhm.num 		= PmsgIhm->num;
			messageFromIhm.data.f[0] 	= PmsgIhm->data.f[0];
			messageFromIhm.data.f[1] 	= PmsgIhm->data.f[1];
			messageFromIhm.data.f[2] 	= PmsgIhm->data.f[2];
			messageFromIhm.data.f[3] 	= PmsgIhm->data.f[3];
			messageFromIhm.data.f[4] 	= PmsgIhm->data.f[4];
			messageFromIhm.data.f[5] 	= PmsgIhm->data.f[5];
			messageFromIhm.data.f[6] 	= PmsgIhm->data.f[6];
			messageFromIhm.data.f[7] 	= PmsgIhm->data.f[7];
			//			msgCode = 1;
			goTo_SinglePosition_old(&messageFromIhm);
			HAL_UART_Transmit(&IHM_UART, (uint8_t *)BufIt2DataPU8, MSG_CPU_SIZE, 50);
			break;
		case 1:			// message from button Start
			messageFromIhm.date 		= PmsgIhm->date;
			messageFromIhm.type[0] 	= PmsgIhm->type[0];
			messageFromIhm.type[1] 	= PmsgIhm->type[1];
			messageFromIhm.type[2] 	= PmsgIhm->type[2];
			messageFromIhm.num 		= PmsgIhm->num;
			messageFromIhm.data.f[0] 	= PmsgIhm->data.f[0];
			messageFromIhm.data.f[1] 	= PmsgIhm->data.f[1];
			messageFromIhm.data.f[2] 	= PmsgIhm->data.f[2];
			messageFromIhm.data.f[3] 	= PmsgIhm->data.f[3];
			messageFromIhm.data.f[4] 	= PmsgIhm->data.f[4];
			messageFromIhm.data.f[5] 	= PmsgIhm->data.f[5];
			messageFromIhm.data.f[6] 	= PmsgIhm->data.f[6];
			messageFromIhm.data.f[7] 	= PmsgIhm->data.f[7];
			messageFromIhm.data.f[8] 	= PmsgIhm->data.f[8];
			//			msgCode = 2;
			flag |= moving;
			break;
		case 2:			// message from button Go to Zero
			//goTo_Zero();
			break;
		}
	}	else if(!strncmp((char *)PmsgIhm->type , "BUS", 3)){ //message de vérification de la communication
		HAL_UART_Transmit(&IHM_UART, (uint8_t *)PmsgIhm, MSG_CPU_SIZE, 100);
		//HAL_UART_Transmit(&IHM_UART, (uint8_t *)PmsgIhm, MSG_CPU_SIZE, 100);
	}	else if(!strncmp((char *)PmsgIhm->type , "OFS", 3)){ //message de l'offset du banc
		offset[0] = PmsgIhm->data.f[0];
		offset[1] = PmsgIhm->data.f[1];
		offset[2] = PmsgIhm->data.f[2];
		offset[3] = PmsgIhm->data.f[3];
	}	else if(!strncmp((char *)PmsgIhm->type , "DIR", 3)){ //message de direction à bouger
		messageFromIhm.date 		= PmsgIhm->date;
		messageFromIhm.type[0] 	= PmsgIhm->type[0];
		messageFromIhm.type[1] 	= PmsgIhm->type[1];
		messageFromIhm.type[2] 	= PmsgIhm->type[2];
		messageFromIhm.num 		= PmsgIhm->num;
		messageFromIhm.data.c[0] 	= PmsgIhm->data.c[0];
		messageFromIhm.data.f[0] 	= PmsgIhm->data.f[0];
		manualControl(&messageFromIhm);
	}
	else { //message pas reconnu demande de réémission du message
		return;
	}

}

uint32_t getDatemsUI32(void)
{
	float timeElapsedU32 = HAL_GetTick() - tickStartU32;
	return setDateValuemsU32 + timeElapsedU32;
}
