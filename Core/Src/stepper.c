/*
 * step.c
 *
 *  Created on: 26 Apr 2021
 *      Author: Samira
 */
/* USER CODE BEGIN 0 */
#include <stepper.h>
#include "main.h"
#include "gpio.h"
#include "tim.h"
#include "dataIhm.h"
#include <math.h>

// External Variables
_Bool stopFlag = 0;
extern unsigned int countX, countY,countZ, countR;
extern uint32_t stepsX, stepsY, stepsZ, stepsR;
extern float id, X, Y, Z, psi, Vx, Vy, Vz, Vr;
extern float x_dist, y_dist, z_dist, r_dist;
extern int x_speed, y_speed, z_speed, r_speed;
extern uint8_t * BufIt2DataPU8;
extern uint32_t ihmMsgLenU32;
extern uint8_t up, down, forward, back, left, right, clockwise, anticlockwise;
extern unsigned int distanceX, distanceY, distanceZ;
extern msgCpu msgPos, messageFromIhm;
extern msgBuff msgBuffer;
extern enum bdtFlags flag;
extern  _Bool X_mouving, Y_mouving, Z_mouving, R_mouving,X_limited, Y_limited, Z_limited, R_limited;
extern  _Bool swFlagTab[];
extern int mainState;
_Bool _swFlagTab[SW_FLAG_LEN]={0};
int tmpFlag; // only for debug



/* BEGIN Functions to Start and Stop Timers in PWM mode with Interrupt */
void accelerateX(void) {
	for(int i=0 ; i <= 20;i++) {
		__HAL_TIM_SET_COUNTER(&htim2,0);
		set_speed_x(0.7*Vx);
		//		// Send position to Operator
		//		msgPos.date = getDatemsUI32();
		//		msgPos.data.f[0] = x_dist;
		//		msgPos.data.f[1] = y_dist;
		//		msgPos.data.f[2] = z_dist;
		//		msgPos.data.f[3] = r_dist;
		//		HAL_UART_Transmit(&huart2, (uint8_t *)&msgPos, MSG_CPU_SIZE,50);
		HAL_Delay(10);
	}
	set_speed_x(Vx);
	//	// Send position to Operator
	//	msgPos.date = getDatemsUI32();
	//	msgPos.data.f[0] = x_dist;
	//	msgPos.data.f[1] = y_dist;
	//	msgPos.data.f[2] = z_dist;
	//	msgPos.data.f[3] = r_dist;
	//	HAL_UART_Transmit(&huart2, (uint8_t *)&msgPos, MSG_CPU_SIZE,50);
}

void decelerateX(void) {
	for(int i=20 ; i >= 0;i--) {
		__HAL_TIM_SET_COUNTER(&htim2,0);
		set_speed_x(0.7*Vx);
		//		// Send position to Operator
		//		msgPos.date = getDatemsUI32();
		//		msgPos.data.f[0] = x_dist;
		//		msgPos.data.f[1] = y_dist;
		//		msgPos.data.f[2] = z_dist;
		//		msgPos.data.f[3] = r_dist;
		//		HAL_UART_Transmit(&huart2, (uint8_t *)&msgPos, MSG_CPU_SIZE,50);
		HAL_Delay(10);
	}
	//	// Send position to Operator
	//	msgPos.date = getDatemsUI32();
	//	msgPos.data.f[0] = x_dist;
	//	msgPos.data.f[1] = y_dist;
	//	msgPos.data.f[2] = z_dist;
	//	msgPos.data.f[3] = r_dist;
	//	HAL_UART_Transmit(&huart2, (uint8_t *)&msgPos, MSG_CPU_SIZE,50);
}

void accelerateY(void) {
	for(int i=0 ; i <= 20;i++) {
		__HAL_TIM_SET_COUNTER(&htim3,0);
		set_speed_y(0.7*Vy);
		HAL_Delay(10);
	}
	set_speed_x(Vx);
}

void decelerateY(void) {
	for(int i=20 ; i >= 0;i--) {
		__HAL_TIM_SET_COUNTER(&htim3,0);
		set_speed_y(0.7*Vy);
		HAL_Delay(10);
	}
}

void accelerateR(void) {
	for(float i=1 ; i < Vr;i+=(0.05*Vr)) {
		__HAL_TIM_SET_COUNTER(&htim5,0);
		set_speed_r(i);
	}
}

void decelerateR(void) {
	for(float i=Vr ; i > 1;i-=(0.05*Vr)) {
		__HAL_TIM_SET_COUNTER(&htim5,0);
		set_speed_r(i);
	}
}
/* Start Separate Axes With Count*/
void startX_PWM(void)
{
	countX = 0;
	X_mouving=1;
	__HAL_TIM_SET_COUNTER(&htim2,0);
	HAL_TIM_PWM_Start_IT(&htim2,TIM_CHANNEL_1);
	memorisePosition();
	//	accelerateX();
}

void startY_PWM(void)
{
	countY = 0;
	Y_mouving=1;
	__HAL_TIM_SET_COUNTER(&htim3,0);
	HAL_TIM_PWM_Start_IT(&htim3,TIM_CHANNEL_1);
	memorisePosition();
	//		accelerateY();
}

void startZ_PWM(void)
{
	countZ = 0;
	Z_mouving=1;
	__HAL_TIM_SET_COUNTER(&htim4,0);
	HAL_TIM_PWM_Start_IT(&htim4,TIM_CHANNEL_1);
	memorisePosition();
}

void startR_PWM(void)
{
	countR = 0;
	R_mouving=1;
	__HAL_TIM_SET_COUNTER(&htim5,0);
	HAL_TIM_PWM_Start_IT(&htim5,TIM_CHANNEL_2);
	memorisePosition();
	//	accelerateR();
}

/* Start Separate Axes No Count*/
void startX(void)
{
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1);
}

void startY(void)
{
	HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1);
}

void startZ(void)
{
	HAL_TIM_PWM_Start(&htim4,TIM_CHANNEL_1);
}

void startR(void)
{
	HAL_TIM_PWM_Start(&htim5,TIM_CHANNEL_2);
}

/* Stop Separate Axes */
void ForceStopX_PWM(void)	//VB
{
		HAL_TIM_PWM_Stop_IT(&htim2,TIM_CHANNEL_1);
		X_mouving=0;
		memorisePosition();

	//	x_dist = 0;
}
void stopX_PWM(void)
{	if (X_state.end_type!=FORCED) {	//VB
	ForceStopX_PWM();
	}
	//	x_dist = 0;
}

void ForceStopY_PWM(void)	//VB
{
		HAL_TIM_PWM_Stop_IT(&htim3,TIM_CHANNEL_1);
		Y_mouving=0;
		memorisePosition();

	//	x_dist = 0;
}
void stopY_PWM(void)
{	if (Y_state.end_type!=FORCED) {	//VB
		ForceStopY_PWM();
	}
	//	y_dist = 0;
}

void ForceStopZ_PWM(void)	//VB
{
		HAL_TIM_PWM_Stop_IT(&htim4,TIM_CHANNEL_1);
		Z_mouving=0;
		memorisePosition();

	//	x_dist = 0;
}

void stopZ_PWM(void)
{	if (Z_state.end_type!=FORCED) {	//VB
		ForceStopZ_PWM();
	}
	//	z_dist = 0;
}


void ForceStopR_PWM(void)	//VB
{
		HAL_TIM_PWM_Stop_IT(&htim5,TIM_CHANNEL_2);
		R_mouving=0;
		memorisePosition();

	//	x_dist = 0;
}

void stopR_PWM(void)
{	if (Z_state.end_type!=FORCED) {	//VB
		HAL_TIM_PWM_Stop_IT(&htim5,TIM_CHANNEL_2);
	}
	//	r_dist = 0;
}

void startAll_PWM(void) {
	startX_PWM();
	startY_PWM();
	startZ_PWM();
	startR_PWM();
}

void stopAll_PWM(void) {
	stopFlag = 1;
	stopX_PWM();
	stopY_PWM();
	stopZ_PWM();
	stopR_PWM();
}


void memorisePosition(void) {

}

void logPosition(void) {
	msgBuffer.msgTab[msgBuffer.inputID].date = getDatemsUI32();
	msgBuffer.msgTab[msgBuffer.inputID].data.f[0] = X_step_to_10Um(X_state.posInStep);
	msgBuffer.msgTab[msgBuffer.inputID].data.f[1] = Y_step_to_10Um(Y_state.posInStep);
	msgBuffer.msgTab[msgBuffer.inputID].data.f[2] = Z_step_to_10Um(Z_state.posInStep);
	msgBuffer.msgTab[msgBuffer.inputID].data.f[3] = R_step_to_10Um(R_state.posInStep);
	msgBuffer.inputID++;
}
/* END Functions to Start and Stop Timers in PWM mode with Interrupt */



int X_mm_to_step(int mm) {
	int steps=0;
	//steps = fabsf(round(X * X_STEPS_PER_MM));
	if (mm<0) mm=-mm;
	steps=(mm*3400)/108;
	return steps;
}
int X_step_to_10Um(int steps) {

	return (steps*2700)/850;
}

int Y_mm_to_step(int mm) {
	int steps=0;
	//steps = fabsf(round(X * Y_STEPS_PER_MM));
	if (mm<0) mm=-mm;
	steps=(mm*4000)/300;
	return steps;
}
int Y_step_to_10Um(int steps) {

	return (steps*300)/40;
}

int Z_mm_to_step(int mm) {
	int steps=0;
	//steps = fabsf(round(X * Y_STEPS_PER_MM));
	if (mm<0) mm=-mm;
	steps=(mm*427)/2;
	return steps;
}
int Z_step_to_10Um(int steps) {

	return (steps*200)/427;
}

int R_mm_to_step(int mm) {
	int steps=0;
	//steps = fabsf(round(X * Y_STEPS_PER_MM));
	if (mm<0) mm=-mm;
	steps=(mm*847)/9;
	return steps;
}
int R_step_to_10Um(int steps) {

	return (steps*9)/847;
}

// Reset Counters
void counter_reset_all(void)
{
	countX = 0, countY = 0, countZ = 0, countR = 0;
	__HAL_TIM_SET_COUNTER(&htim2,0);
	__HAL_TIM_SET_COUNTER(&htim3,0);
	__HAL_TIM_SET_COUNTER(&htim4,0);
	__HAL_TIM_SET_COUNTER(&htim5,0);
}
/*
 * 	BEGIN Functions for managing motor directions depending on axis
 */

/**********	X- AXIS	*****************/
void backwardX(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_SET);
	back = 1;
	forward = 0;
}

void forwardX(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, GPIO_PIN_SET);
	back = 0;
	forward = 1;
}

// Reverse X-axis no matter current direction
void reverseX(void) {
	//HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_4)));
	//HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_3)));
	if (back) {	forwardX();}
	else {backwardX();}
}

// Reverse motors separately
void reverseX1(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_3, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_3)));
}
void reverseX2(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_4, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_4)));
}

/**********	Y - AXIS *****************/

void leftY(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7, GPIO_PIN_SET);
	left = 1;
	right = 0;
}

void rightY(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7, GPIO_PIN_RESET);
	left = 0;
	right = 1;
}

// Reverse Y-axis no matter current direction
void reverseY(void) {
	//HAL_GPIO_WritePin(GPIOD, GPIO_PIN_7, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_7)));
	if (left) {	rightY();}
	else {leftY();}
}

/**********	Z - AXIS *****************/

void upZ(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_6, GPIO_PIN_RESET); // SET
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, GPIO_PIN_RESET);
	up = 1;
	down = 0;
}

void downZ(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, GPIO_PIN_SET);
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_6, GPIO_PIN_SET); //RESET
	down = 1;
	up = 0;
}

// Reverse Z-axis no matter current direction
void reverseZ_old(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_6, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_6)));
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_5)));
}
void reverseZ(void) {
	if (down) {	upZ();}
	else {downZ();}
}
void reverseZ1(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_5, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_5)));
}
void reverseZ2(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_6, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_6)));
}

/**********	ROTATION *****************/

void clockwiseR(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_RESET);
	clockwise=1;
	anticlockwise=0;
}

void anticlockwiseR(void) {
	HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, GPIO_PIN_SET);
	clockwise=0;
	anticlockwise=1;
}

// Reverse Rotation no matter current direction
void reverseR(void) {
	//HAL_GPIO_WritePin(GPIOD, GPIO_PIN_1, !(HAL_GPIO_ReadPin(GPIOD,GPIO_PIN_1)));
	if (clockwise) {	anticlockwiseR();}
	else {clockwiseR();}
}

/*
 * 	END Functions for managing motor directions depending on axis
 */

/*
 *  BEGIN Functions for setting motor speed
 */
void set_speed_x(float Vx) {
	X_state.vitesseCurrent=Vx;
	x_speed = 64000000 / (X_STEPS_PER_MM * Vx) - 1;
	//if (x_speed>0xffff) x_speed=0xffff;
	__HAL_TIM_SET_AUTORELOAD(&htim2, x_speed);						// TIMx_ARR for period
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, (x_speed/2));		// TIMx_CCx for pulse width. 50% pulse width
}

void set_speed_y(float Vy) {
	Y_state.vitesseCurrent=Vy;
	y_speed = 32000000 / (Y_STEPS_PER_MM * Vy) - 1;
	if (y_speed>0xffff) y_speed=0xffff;
	__HAL_TIM_SET_AUTORELOAD(&htim3, y_speed);
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (y_speed/2));
}

void set_speed_z(float Vz) {
	Z_state.vitesseCurrent=Vz;
	z_speed = 32000000 / (Z_STEPS_PER_MM * Vz) - 1;
	if (z_speed>0xffff) z_speed=0xffff;
	__HAL_TIM_SET_AUTORELOAD(&htim4, z_speed);
	__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, (z_speed/2));
}

void set_speed_r(float Vr) {
	R_state.vitesseCurrent=Vr;
	r_speed = 64000000 / (R_STEPS_PER_REV * Vr) - 1;
	if (r_speed>0xffff) r_speed=0xffff;
	__HAL_TIM_SET_AUTORELOAD(&htim5, r_speed);
	__HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_2, (r_speed/2));
}



// Set the speed of all motors as per file values

void set_speed_all(float Vx, float Vy, float Vz, float Vr) {
	set_speed_x(Vx);
	set_speed_y(Vy);
	set_speed_z(Vz);
	set_speed_r(Vr);
}
void set_speed_all_old(float Vx, float Vy, float Vz, float Vr) {
	// Speed equivalent in mm/s converted to CPU value
	x_speed = 64000000 / (X_STEPS_PER_MM * Vx) - 1;
	y_speed = 32000000 / (Y_STEPS_PER_MM * Vy) - 1;
	z_speed = 32000000 / (Z_STEPS_PER_MM * Vz) - 1;
	r_speed = 64000000 / (R_STEPS_PER_REV * Vr) - 1;

	//	 Send new speed values to motor timers
	__HAL_TIM_SET_AUTORELOAD(&htim2, x_speed);						// TIMx_ARR for period
	__HAL_TIM_SET_COMPARE(&htim2, TIM_CHANNEL_1, (x_speed/2));		// TIMx_CCx for pulse width. 50% pulse width

	__HAL_TIM_SET_AUTORELOAD(&htim3, y_speed);
	__HAL_TIM_SET_COMPARE(&htim3, TIM_CHANNEL_1, (y_speed/2));

	__HAL_TIM_SET_AUTORELOAD(&htim4, z_speed);
	__HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, (z_speed/2));

	__HAL_TIM_SET_AUTORELOAD(&htim5, r_speed);
	__HAL_TIM_SET_COMPARE(&htim5, TIM_CHANNEL_2, (r_speed/2));
}

void reset_distances(void) {
	x_dist = 0;
	y_dist = 0;
	z_dist = 0;
	r_dist = 0;
}
/*
 * END Functions for setting motor speed
 */

/*
 * 	BEGIN Functions for High level Machine State	//VB
 */

void X_sate_machine(){
	int sw_i=0;
	int BackDist=10;
	static int antiShiftDir_countX1,antiShiftDir_countX2;

	int mouving=X_mouving;

	motorState * state= & X_state;
	static int lastMouving;					// a chaque changement d'etat "deplacement" on envoie la position
	if (lastMouving!=mouving){
		sendPos();
		lastMouving=mouving;
	}

	// gestion de l'accelleration
	if (mainState==Mouving){
		int Count=countX, Step=stepsX, refreshDist=0;
		refreshDist=X_mm_to_step(10);
		int nbAcc=0,nbDec=0;
		static int nbAccOld,nbDecOld;
		int vitAcc=0,vitDec=0,vitFinal=0;

		nbAcc=Count/refreshDist;
		nbDec=(Step-Count)/refreshDist;
		if ( nbAcc!=nbAccOld || nbDec!=nbDecOld){
			vitAcc=nbAcc*state->vitesseIncrement+state->vitesseCanStart;
			nbAccOld=nbAcc;
			vitDec=nbDec*state->vitesseIncrement+state->vitesseCanStart;
			nbDecOld=nbDec;
			// prendre la plus petite vitesse
			vitFinal=state->vitesseTarget;
			if (vitFinal>vitAcc) vitFinal=vitAcc;
			if (vitFinal>vitDec) vitFinal=vitDec;
			// si la vitesse doit changé
			if (state->vitesseCurrent!=vitFinal){
				state->vitesseCurrent=vitFinal;
				set_speed_x(state->vitesseCurrent);
				sendPos();
			}



		}






	}

	switch (state->state){
	case Start:
		if (state->dir==0) { 	backwardX();	}
		else {	forwardX();		}
		switch (state->end_type){
				case SWITCH:
					state->vitesseCurrent=state->vitesseTarget;
					set_speed_x(state->vitesseCurrent);
					X_limited=0;
					startX_PWM();
					state->state=Run;
					break;
				case FORCED:
				case DISTANCE:
					if(state->vitesseTarget > state->vitesseCanStart){  // demarrage avec acceleration
						state->vitesseCurrent=state->vitesseCanStart;
					}
					else  {
						state->vitesseCurrent=state->vitesseTarget;
					}
					set_speed_x(state->vitesseCurrent);
					stepsX=X_mm_to_step(state->dist);
					X_limited=1;
					startX_PWM();
					state->state=Run;
					break;

		}
		break;
	case Run:

		// SW_X1SEC=1,SW_X2SEC,SW_X1MAX,SW_X1MIN,SW_X2MAX,SW_X2MIN

		for (sw_i=SW_X1SEC;sw_i<=SW_X2MIN;sw_i++){
			if(_swFlagTab[sw_i]==1) {

				if (state->end_type!=FORCED) {
					switch (sw_i){
					case SW_X1SEC:
					case SW_X2SEC:
					case SW_X1MIN:
					case SW_X2MIN:
						forwardX();
						break;
					case SW_X1MAX:
					case SW_X2MAX:
						backwardX();
					}
					set_speed_x(10);
					X_limited=1;
					state->end_type=FORCED;
					stepsX=X_mm_to_step(BackDist); // distance à reculer en securité
					startX_PWM();
					mouving=1;
					state->state=BackStep1;
				}
			}
			_swFlagTab[sw_i]=0;
		}
		if (mouving==0) {
			state->state=End;
		}
		break;
	case BackStep1:
		if (mouving==0) {
			reverseX();
			X_limited=0;
			state->end_type=SWITCH;
			startX_PWM();

			state->state=BackStep2;
		}
		break;
	case BackStep2:
		if (mouving==0) {

			reverseX();
			X_limited=1;
			state->end_type=FORCED;
			stepsX=X_mm_to_step(BackDist); // distance à reculer en securité
			startX_PWM();
			state->state=BackStep3;

		}
		break;
	case BackStep3:
		if (mouving==0) {
			if (_swFlagTab[SW_X2MIN]==1 || _swFlagTab[SW_X1MIN]==1){
				if (_swFlagTab[SW_X1MIN]==1 ){			// premiere buté trouvé, on revenu en arriere de BackDist
					//antiShiftDir=1;
					reverseX2();
				}
				else{
					//antiShiftDir=2;
					reverseX1();
				}											// le moteur qui a trouvé la buté recule , l'autre peux avancer
				for (sw_i=SW_X1SEC;sw_i<=SW_X2MIN;sw_i++) {
					swFlagTab[sw_i]=_swFlagTab[sw_i];
					_swFlagTab[sw_i]=0;
				}
				antiShiftDir_countX1=countX;
				X_limited=1;
				state->end_type=DISTANCE;
				stepsX=X_mm_to_step(50); // distance reglable
				startX_PWM();
				state->state=AntiShift1;
			}
			else {
				state->state=End;
				state->posInStep=0;
				for (sw_i=SW_X1SEC;sw_i<=SW_X2MIN;sw_i++) {
					swFlagTab[sw_i]=_swFlagTab[sw_i];
					_swFlagTab[sw_i]=0;
				}
			}
		}
		break;

	case AntiShift1:
		if (mouving==0) {												// on a trouvé la deuxieme buté ou on est arreter par securité
			for (sw_i=SW_X1SEC;sw_i<=SW_X2MIN;sw_i++) {
				swFlagTab[sw_i]=_swFlagTab[sw_i];
				_swFlagTab[sw_i]=0;
			}
			antiShiftDir_countX2=countX;
			reverseX1();
			reverseX2();

			X_limited=1;												// on recalle les deux moteurs avec la différence de pas entre les deux divisé par deux
			state->end_type=FORCED;
			stepsX=(antiShiftDir_countX2+antiShiftDir_countX1)/2; //  calcule ecart a reprendre
			startX_PWM();
			state->state=AntiShift2;
		}
		break;
	case AntiShift2:
		if (mouving==0) {												// on a recalé l'ecart entre les moteurs
			for (sw_i=SW_X1SEC;sw_i<=SW_X2MIN;sw_i++) {
				swFlagTab[sw_i]=_swFlagTab[sw_i];
				_swFlagTab[sw_i]=0;
			}
			X_limited=0;
			backwardX();									// on recherche une buté
			state->end_type=SWITCH;
			startX_PWM();
			state->state=AntiShift3;
		}
		break;
	case AntiShift3:												// on s'ecarte de la buté
		if (mouving==0) {
			forwardX();
			X_limited=1;
			state->end_type=FORCED;
			stepsX=X_mm_to_step(BackDist); // distance reglable
			startX_PWM();
			state->state=AntiShift4;
		}
		break;
	case AntiShift4:
		if (mouving==0) {
			state->state=End;						// on nettoie
			state->posInStep=0;
			for (sw_i=SW_X1SEC;sw_i<=SW_X2MIN;sw_i++) {
				swFlagTab[sw_i]=_swFlagTab[sw_i];
				_swFlagTab[sw_i]=0;
			}
		}
		break;
	case End:
	case Stop:

		break;
	default:
		break;
	}
}


void X_run(_Bool dir, int end_type ,int dist, int vitesse ){
	motorState * state =& X_state;
	state->dir=dir;
	state->end_type=end_type;
	state->dist=dist;
	state->vitesseTarget=vitesse;

	if (dir==0)
	{ 	backwardX();
	}
	else {
		forwardX();
	}

	switch (end_type){
		case SWITCH:
			state->state=Start;
			break;
		case FORCED:
			if (vitesse>30) state->vitesseTarget=30;
			if (dist>50) state->dist=50;
			state->state=Start;
			break;
		case DISTANCE:
			if (state->dist==0){
				state->state=End;
			} else {
				state->state=Start;
			}
			break;
	}
}

void Y_sate_machine(){
	int sw_i=0;
	int BackDist=10;

	int mouving=Y_mouving;  // mouving is memorised, so he can't be changed by IRQ during many state decision
	motorState * state= & Y_state;
	static int lastMouving;
	if (lastMouving!=mouving){
		sendPos();
		lastMouving=mouving;
	}
	switch (state->state){
	case Start:
		if (state->dir==0) { 	leftY();	}
		else {	rightY();		}
		switch (state->end_type){
				case SWITCH:
					set_speed_y(state->vitesseTarget);
					Y_limited=0;
					startY_PWM();
					state->state=Run;
					break;
				case FORCED:
				case DISTANCE:
					set_speed_y(state->vitesseTarget);
					//set_speed_x(state->dist);
					stepsY=Y_mm_to_step(state->dist);
					Y_limited=1;
					startY_PWM();
					state->state=Run;
					break;
		}
		break;
	case Run:

		// SW_Y1SEC=1,SW_X2SEC,SW_X1MAX,SW_X1MIN,SW_X2MAX,SW_X2MIN

		for (sw_i=SW_YMAX;sw_i<=SW_YMIN;sw_i++){
			if(_swFlagTab[sw_i]==1) {

				if (state->end_type!=FORCED) {
					switch (sw_i){
					case SW_YMIN:
						rightY();
						break;
					case SW_YMAX:
						leftY();
					}
					set_speed_y(30);  // bug si trop petit !!!!
					Y_limited=1;
					state->end_type=FORCED;
					stepsY=Y_mm_to_step(BackDist); // distance à reculer en securité
					startY_PWM();
					mouving=1;
					state->state=BackStep1;
				}
			}
			_swFlagTab[sw_i]=0;
		}
		if (mouving==0) {
			state->state=End;
		}
		break;
	case BackStep1:
		if (mouving==0) {
			reverseY();
			Y_limited=0;
			startY_PWM();
			state->end_type=SWITCH;
			state->state=BackStep2;
		}
		break;
	case BackStep2:
		if (mouving==0) {
			reverseY();
			Y_limited=1;
			state->end_type=FORCED;
			stepsY=Y_mm_to_step(BackDist); // distance à reculer en securité
			startY_PWM();
			state->state=BackStep3;
		}
		break;
	case BackStep3:
		if (mouving==0) {
			state->state=End;
			state->posInStep=0;
			for (sw_i=SW_YMAX;sw_i<=SW_YMIN;sw_i++) {
				swFlagTab[sw_i]=_swFlagTab[sw_i];
				_swFlagTab[sw_i]=0;
			}
		}
		break;
	case End:
	case Stop:
		break;
	default:
		break;
	}
}


void Y_run(_Bool dir, int end_type ,int dist, int vitesse ){
	motorState * state =& Y_state;
	state->dir=dir;
	state->dir=dir;
	state->end_type=end_type;
	state->dist=dist;
	state->vitesseTarget=vitesse;

	if (dir==0)
	{ 	leftY();
	}
	else {
		rightY();
	}

	switch (end_type){
		case SWITCH:
			state->state=Start;
			break;
		case FORCED:
			//if (vitesse>30) state->vitesse=30;
			if (dist>50) state->dist=50;
			state->state=Start;
			break;
		case DISTANCE:
			if (state->dist==0){
				state->state=End;
			} else {
				state->state=Start;
			}
			break;
	}
}



void Y_run_old(_Bool dir, int end_type ,int dist, int vitesse ){
	Y_state.dir=dir;
	Y_state.end_type=end_type;
	Y_state.dist=dist;
	Y_state.vitesseTarget=vitesse;

	switch (end_type){
		case SWITCH:
			set_speed_y(vitesse);
			Y_limited=0;
			if (dir==0) { leftY();	}
			else {rightY();}
			startY_PWM();
			Y_state.state=Run;
			break;
	}
}


void Z_sate_machine(){
	int sw_i=0;
	int BackDist=10;

	int mouving=Z_mouving;  // mouving is memorised, so he can't be changed by IRQ during many state decision
	motorState * state= & Z_state;
	static int lastMouving;
	if (lastMouving!=mouving){
		sendPos();
		lastMouving=mouving;
	}
	switch (state->state){
	case Start:
		if (state->dir==0) { 	upZ();	}
		else {	downZ();		}
		switch (state->end_type){
				case SWITCH:
					set_speed_z(state->vitesseTarget);
					Z_limited=0;
					startZ_PWM();
					state->state=Run;
					break;
				case FORCED:
				case DISTANCE:
					set_speed_z(state->vitesseTarget);
					//set_speed_x(state->dist);
					stepsZ=Z_mm_to_step(state->dist);
					Z_limited=1;
					startZ_PWM();
					state->state=Run;
					break;
		}
		break;
	case Run:

		// SW_Y1SEC=1,SW_X2SEC,SW_X1MAX,SW_X1MIN,SW_X2MAX,SW_X2MIN

		for (sw_i=SW_Z1MAX;sw_i<=SW_Z2MIN;sw_i++){
			if(_swFlagTab[sw_i]==1) {

				//tmpFlag=sw_i;//_swFlagTab
				if (state->end_type!=FORCED) {
					switch (sw_i){
					case SW_Z2MIN:
					case SW_Z1MIN:
						upZ();
						break;
					case SW_Z1MAX:
					case SW_Z2MAX:
						downZ();
					}
					set_speed_z(10);
					Z_limited=1;
					state->end_type=FORCED;
					stepsZ=Z_mm_to_step(BackDist); // distance à reculer en securité
					startZ_PWM();
					mouving=1;
					state->state=BackStep1;
				}
			}
			_swFlagTab[sw_i]=0;
		}
		if (mouving==0) {
			state->state=End;
		}
		break;
	case BackStep1:
		if (mouving==0) {
			reverseZ();
			Z_limited=0;
			startZ_PWM();
			state->end_type=SWITCH;
			state->state=BackStep2;
		}
		break;
	case BackStep2:
		if (mouving==0) {
			reverseZ();
			Z_limited=1;
			state->end_type=FORCED;
			stepsZ=Z_mm_to_step(BackDist); // distance à reculer en securité
			startZ_PWM();
			state->state=BackStep3;
		}
		break;
	case BackStep3:
		if (mouving==0) {
			state->state=End;
			state->posInStep=0;
			for (sw_i=SW_Z1MAX;sw_i<=SW_Z2MIN;sw_i++) {
				swFlagTab[sw_i]=_swFlagTab[sw_i];
				_swFlagTab[sw_i]=0;
			}
		}
		break;
	case End:
	case Stop:
		break;
	default:
		break;
	}
}


void Z_run(_Bool dir, int end_type ,int dist, int vitesse ){
	motorState * state =& Z_state;

	state->dir=dir;
	state->end_type=end_type;
	state->dist=dist;
	state->vitesseTarget=vitesse;

	if (dir==0)
	{ 	upZ();
	}
	else {
		downZ();
	}

	switch (end_type){
		case SWITCH:
			state->state=Start;
			break;
		case FORCED:
			if (vitesse>10) state->vitesseTarget=10;
			if (dist>50) state->dist=50;
			state->state=Start;
			break;
		case DISTANCE:
			if (state->dist==0){
				state->state=End;
			} else {
				state->state=Start;
			}
			break;
	}
}


void Z_run_old(_Bool dir, int end_type ,int dist, int vitesse ){
	Z_state.dir=dir;
	Z_state.end_type=end_type;
	Z_state.dist=dist;
	Z_state.vitesseTarget=vitesse;

	switch (end_type){
		case SWITCH:
			set_speed_z(vitesse);
			Z_limited=0;
			if (dir==0) { upZ();	}
			else {downZ();}
			startZ_PWM();
			Y_state.state=Run;
			break;
	}
}

void R_run(_Bool dir, int end_type ,int dist, int vitesse ){
	R_state.dir=dir;
	R_state.end_type=end_type;
	R_state.dist=dist;
	R_state.vitesseTarget=vitesse;


	switch (end_type){
		case SWITCH:
			set_speed_r(vitesse);
			R_limited=0;
			if (dir==0) { upZ();	}
			else {clockwiseR();}
			anticlockwiseR();
			R_state.state=Run;
			break;
	}
}


_Bool isInSecureMode(void){
	_Bool isSecure =0;
	int i;
	motorState * state;
	for (i=0;i<4;i++){
		switch (i){
		case 0:state= & X_state;
			break;
		case 1:state= & Y_state;
			break;
		case 2:state= & Z_state;
			break;
		case 3:state= & R_state;
			break;
		}
		switch (state->state){
			case BackStep1:
			case BackStep2:
			case BackStep3:
				isSecure =1;
				break;
			default:
				break;
		}
	}
	return isSecure;
}

/*
 * END Functions for High level Machine State
 */

/*
 * 	BEGIN Functions for managing Messages from HMI
 */

//	Go to Start or End position depending on message from HMI
void goTo_Position() {
	msgCpu* PmsgIhm=&messageFromIhm;

	//stopFlag = 0;	//Set stop flag to 0 in case it is still at 1
	X = PmsgIhm->data.f[1];					// X distance en mm
	Y = PmsgIhm->data.f[2];					// Y distance en mm
	Z = PmsgIhm->data.f[3];					// Z distance en mm
	psi = PmsgIhm->data.f[4];				// Rotation in degrees
	// Vitesses en mm/s
	Vx = PmsgIhm->data.f[5];
	Vy = PmsgIhm->data.f[6];
	Vz = PmsgIhm->data.f[7];
	Vr = PmsgIhm->data.f[8];


	All_runPosition();

//
}

void goToSinglePosition() {
	msgCpu* PmsgIhm=&messageFromIhm;

	X = PmsgIhm->data.f[0];					// X distance en mm
	Y = PmsgIhm->data.f[1];					// Y distance en mm
	Z = PmsgIhm->data.f[2];					// Z distance en mm
	psi = PmsgIhm->data.f[3];				// Rotation in degrees
	// Vitesses en mm/s
	Vx = PmsgIhm->data.f[4];
	Vy = PmsgIhm->data.f[5];
	Vz = PmsgIhm->data.f[6];

	All_runPosition();



//
}

void All_runPosition(void){
	X_run(X > 0, DISTANCE ,X, (int) Vx);
	Y_run(Y > 0, DISTANCE ,Y, (int) Vy)	;
	Z_run(Z > 0, DISTANCE ,Z, (int) Vz);

}


void goTo_SinglePosition_old(msgCpu* PmsgIhm) {
	stopFlag = 0;	//Set stop flag to 0 in case it is still at 1
	X = PmsgIhm->data.f[0];					// X distance en mm
	Y = PmsgIhm->data.f[1];					// Y distance en mm
	Z = PmsgIhm->data.f[2];					// Z distance en mm
	psi = PmsgIhm->data.f[3];				// Rotation in degrees
	// Vitesses en mm/s
	Vx = PmsgIhm->data.f[4];
	Vy = PmsgIhm->data.f[5];
	Vz = PmsgIhm->data.f[6];
	Vr = PmsgIhm->data.f[7];

	set_speed_all(Vx, Vy, Vz, Vr);

	//	//X-axis direction check
	if(X <= 0) {
		backwardX();
	} else forwardX();
	//Y-axis direction check
	if(Y <= 0) {
		leftY();
	} else rightY();
	//Z-axis direction check
	if(Z <= 0) {
		upZ();
	} else downZ();
	//Rotation direction check
	if(psi <= 0) {
		anticlockwiseR();
	} else clockwiseR();

	stepsX = fabsf(round(X * X_STEPS_PER_MM));
	stepsY = fabsf(round(Y * Y_STEPS_PER_MM));
	stepsZ = fabsf(round(Z * Z_STEPS_PER_MM));
	stepsR = fabsf(round(psi * R_STEPS_PER_REV));
	X_limited=0;
	Y_limited=0;
	Z_limited=0;
	R_limited=0;

	if(stepsX != 0) {
		startX_PWM();
	}
	if(stepsY != 0) {
		startY_PWM();
	}
	if(stepsZ != 0) {
		startZ_PWM();
	}
	if(stepsR != 0) {
		startR_PWM();
	}
	//	counter_reset_all();
	// Wait for required count for all axes before exiting
	while(!(countX >= stepsX && countY >= stepsY && countZ >= stepsZ && countR >= stepsR)) {
		if(stopFlag == 1) {
			stopFlag = 0;
			return;
		}
		//		if(countX >= (0.8*stepsX)) {
		//			decelerateX();
		//		}
	}
}

//	Go to position received from HMI
void goToNextPosition(msgCpu* PmsgIhm) {
	id = PmsgIhm->data.f[0];
	X = PmsgIhm->data.f[1];
	Y = PmsgIhm->data.f[2];
	Z = PmsgIhm->data.f[3];
	psi = PmsgIhm->data.f[4];
	Vx = PmsgIhm->data.f[5];
	Vy = PmsgIhm->data.f[6];
	Vz = PmsgIhm->data.f[7];
	Vr = PmsgIhm->data.f[8];

	set_speed_all(Vx, Vy, Vz, Vr);

	//	X-axis direction check
	if(X <= 0) {
		backwardX();
	} else forwardX();
	//Y-axis direction check
	if(Y <= 0) {
		leftY();
	} else rightY();
	//Z-axis direction check
	if(Z <= 0) {
		upZ();
	} else downZ();
	//Rotation direction check
	if(psi <= 0) {
		anticlockwiseR();
	} else clockwiseR();
	// Set steps and count until required steps reached, then stop motors
	//	stepsX = fabsf(round(X * X_STEPS_PER_MM));
	stepsX = fabsf((X*3400)/108);
	stepsY = fabsf(round(Y * Y_STEPS_PER_MM));
	stepsZ = fabsf(round(Z * Z_STEPS_PER_MM));
	stepsR = fabsf(round(psi * R_STEPS_PER_REV));
	X_limited=0;
	Y_limited=0;
	Z_limited=0;
	R_limited=0;
//	int tx = X/Vx; //total time in s
//	int ty = Y/Vy;
//	int tz = Z/Vz;
//	int tr = R/Vr;
	if(stepsX != 0) {
		startX_PWM();
	}
	if(stepsY != 0) {
		startY_PWM();
	}
	if(stepsZ != 0) {
		startZ_PWM();
	}
	if(stepsR != 0) {
		startR_PWM();
	}
	//	counter_reset_all();

	// Wait for required count for all axes before exiting   // VB
//	while(!(countX >= stepsX && countY >= stepsY && countZ >= stepsZ && countR >= stepsR)) {
//		int t = HAL_GetTick() - startTime;

		//		if(flag & message)
		//		{
		//			// Send position to Operator
		//			msgPos.date = getDatemsUI32();
		//			msgPos.data.f[0] = x_dist;
		//			msgPos.data.f[1] = y_dist;
		//			msgPos.data.f[2] = z_dist;
		//			msgPos.data.f[3] = r_dist;
		//			HAL_UART_Transmit(&huart2, (uint8_t *)&msgPos, MSG_CPU_SIZE,50);
		//		// HERE NEED TO SEND MESSAGE THAT BENCH STOPPED IN MIDDLE OF TRAJECTORY message d'urgence peut-etre

		//		if(countX >= (0.8*stepsX)) {
		//			decelerateX();
		//		}
//	}
}
// VB
_Bool checkPosition(void){
		return (countX >= stepsX && countY >= stepsY && countZ >= stepsZ && countR >= stepsR);
}

//	Go to Zero / Home Function
void goTo_Zero(void)
{
	upZ(), leftY(), backwardX();			// Change all motor directions to face home
	startX();
	startY();
	startZ();									// Start motors
	// End of travel sstopAll_PWMwitches will deal with end of travel reaction
}


//VB

void manualState(msgCpu* PmsgIhm) {
	// Default speed settings for manual control

	switch (PmsgIhm->num) {
	case 0:									// X-axis forward/backward control

		if(PmsgIhm->data.c[0] == 'F') {
			X_run(FORWARD, SWITCH ,0, 30);	}
		if(PmsgIhm->data.c[0] == 'B') {
			X_run(BACKWARD, SWITCH ,0, 30 );
		}
		break;
	case 1:									// Y-axis left/right control
		if(PmsgIhm->data.c[0] == 'L') {
			Y_run(LEFTY, SWITCH ,0, 50)	;
		}
		if(PmsgIhm->data.c[0] == 'R') {
			Y_run(RIGHTY, SWITCH ,0, 50);
		}

		break;
	case 2:									// Z-axis up/down control
		Z_limited=0;
		if(PmsgIhm->data.c[0] == 'U') {
			Z_run(UPZ, SWITCH ,0, 10);
		}
		if(PmsgIhm->data.c[0] == 'D') {
			Z_run(DOWNZ, SWITCH ,0, 10);
		}

		break;
	case 3:									// Rotation clockwise/anti-clockwise control
		R_limited=0;
		if(PmsgIhm->data.c[0] == 'C') {
			Z_run(CLOCKWISER, SWITCH ,0, 0);
		}
		if(PmsgIhm->data.c[0] == 'A') {
			Z_run(ANTICLOCKWISER, SWITCH ,0, 0);
		}

		break;
	case 4:									// Stopping control per axis or altogether
		if(PmsgIhm->data.f[0] == 0) {
			ForceStopX_PWM();
			X_state.state=Stop;
		} else if(PmsgIhm->data.f[0] == 1) {
			ForceStopY_PWM();
			Y_state.state=Stop;
		} else if(PmsgIhm->data.f[0] == 2) {
			ForceStopZ_PWM();
			Z_state.state=Stop;
		} else if(PmsgIhm->data.f[0] == 3) {
			ForceStopR_PWM();
			R_state.state=Stop;
		} else if(PmsgIhm->data.f[0] == 4) {	// Stop command for all motors
			stopAll_PWM();			// ne semble pas entrer dans ce cas
			X_state.state=Stop;
			Y_state.state=Stop;
			Z_state.state=Stop;
			R_state.state=Stop;
		}
		break;
	default :
		stopAll_PWM();
		break;
	}
}

/*		MANUAL DIRECTIONAL COMMANDS		*/
void manualControl(msgCpu* PmsgIhm) {
	// Default speed settings for manual control
	Vx = 30;
	Vy = 50;
	Vz = 10;
	set_speed_all(Vx, Vy, Vz, Vr);
	switch (PmsgIhm->num) {
	case 0:									// X-axis forward/backward control
		X_limited=0;
		if(PmsgIhm->data.c[0] == 'F') {
			forwardX();
		}
		if(PmsgIhm->data.c[0] == 'B') {
			backwardX();
		}
		startX();
		break;
	case 1:									// Y-axis left/right control
		Y_limited=0;
		if(PmsgIhm->data.c[0] == 'L') {
			leftY();
		}
		if(PmsgIhm->data.c[0] == 'R') {
			rightY();
		}
		startY_PWM();
		break;
	case 2:									// Z-axis up/down control
		Z_limited=0;
		if(PmsgIhm->data.c[0] == 'U') {
			upZ();
		}
		if(PmsgIhm->data.c[0] == 'D') {
			downZ();
		}
		startZ_PWM();
		break;
	case 3:									// Rotation clockwise/anti-clockwise control
		R_limited=0;
		if(PmsgIhm->data.c[0] == 'C') {
			clockwiseR();
		}
		if(PmsgIhm->data.c[0] == 'A') {
			anticlockwiseR();
		}
		startR_PWM();
		break;
	case 4:									// Stopping control per axis or altogether
		if(PmsgIhm->data.f[0] == 0) {
			stopX_PWM();
		} else if(PmsgIhm->data.f[0] == 1) {
			stopY_PWM();
		} else if(PmsgIhm->data.f[0] == 2) {
			stopZ_PWM();
		} else if(PmsgIhm->data.f[0] == 3) {
			stopR_PWM();
		} else if(PmsgIhm->data.f[0] == 4) {	// Stop command for all motors
			stopAll_PWM();
		}
		break;
	default :
		stopAll_PWM();
		break;
	}
}
/*
 * 	END Functions for managing Messages from HMI
 */

/* USER CODE END 0 */
